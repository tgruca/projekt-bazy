#!/usr/bin/bash

# Switch dir
cd db
# echo %srcdir%

# Run sql scripts
mysql -e "source pizzeria.sql"

# Return back to root dir
cd ..