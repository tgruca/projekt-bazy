import PyQt5.QtWidgets as qt
import core
import ui
import ctrl


class MainCtrl(ctrl.Controller):
    """Main UI Controller Class
    """

    def __init__(self, app, ui):
        """Create MainCtrl object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        super().__init__(app, ui)
        self.setup()

    def setup(self):
        """Configure UI with application
        """
        self._ui.createLayout(self._app._db)
        # pass

class MainMenuCtrl(ctrl.Controller):
    """Main menu UI Controller Class
    """

    def __init__(self, app, ui):
        """Create Main menu controller

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        super().__init__(app, ui)
        self.setup()

    def setup(self):
        """Configure UI with application
        """
        # self._ui.createLayout(self._app._db)

        # Connect functions to the buttons
        # When 'wyloguj' button is pressed
        self._ui.buttons['wyloguj'].clicked.connect(self._logout_fn)
        self._ui.buttons['menu'].clicked.connect(self._menu_fn)
        self._ui.buttons['produkty'].clicked.connect(self._produkty_fn)
        self._ui.buttons['zamowienia'].clicked.connect(self._zamowienia_fn)

        

    def _logout_fn(self):
        """Log out from application.
        Returns user to login screen.
        """
        print("[D] log out")
        # Set to login UI
        self._app.setUI('login')

    def _produkty_fn(self):
        """Switch to 'produkty' screen.
        """
        # print("Clicked 'produkty' button")
        self._app.setUI('produkty')

    def _menu_fn(self):
        """Switch to 'menu' screen.
        """
        # print("Clicked 'menu' button")
        self._app.setUI('menu')
    
    def _zamowienia_fn(self):
        """Switch to 'zamowienia' screen.
        """
        # print("Clicked 'zamowienie' button")
        self._app.setUI('zamowienie')
        

