import PyQt5.QtWidgets as qt
import PyQt5.QtCore as qtcore
import PyQt5.QtGui as qtgui
import core
import ui
import styles
from functools import partial
import ctrl
import atexit


class MenuCtrl(ctrl.Controller):
    """Menu UI controller class.
    """

    def __init__(self, app, ui):
        """Create MainCtrl object

        Args:
            app (App): handle to App object
            ui (QWidget): handle to ui object
        """
        super().__init__(app, ui)
        self.edit_mode = False
        self.setup()
        atexit.register(self.cleanup)

    def cleanup(self):
        # remove order if opened
        if self.currentOrderId is not None:
            if float(self._app._user._db.query(f"select wartoscrachunku from zamowienia where idzamowienia={self.currentOrderId};")[0][0]) == 0.0:
                self.removeOrder(self.currentOrderId)
            self.currentOrderId = None

    def setup(self):
        """Configure UI with application
        """
        self._ui.createLayout(self.getMenuItems())
        # Connect edit mode buttons
        self.connectEditModeButton()

        self.currentOrderId = None
        self._ui.buttons['zrealizuj'].clicked.connect(self.openPaymentWidget)
        self._ui.buttons['go_back'].clicked.connect(self.go_backHandle)
        self._ui.buttons['orders'].clicked.connect(self.openOrderManagementWidget)
        self._ui.buttons['new-order'].clicked.connect(self.createNewOrder)

        if self._app._user._admin == False:
            self._ui.buttons['edit'].hide()


        # Set style settings from 'app.conf'
        self._ui.setStyleSheet(
            f'font-size: {self._app._settings["ui"]["menu-font-size"]};')

    def menuItemClicked(self, item_id):
        q = self._app._user._db.query(
            "select nazwa, cena from produkty where idproduktu={}".format(item_id))
        self.addOrderItem(item_id, ui.OrderItem(
            item_id, q[0][0], str(q[0][1]), colors=self._app._settings['ui']['color']))

    def getMenuItems(self):
        # Fetch product names
        q = self._app._user._db.query(
            'select idproduktu, nazwa, cena, idtypu from produkty;')

        qq = self._app._user._db.query('select * from typproduktu')
        cat = {}
        items = {}
        for row in qq:
            cat[row[0]] = row[1]
            items[row[1]] = []

        # print(items)
        # print(qq)
        # Create Produkt objects
        for row in q:
            menu_item = ui.menu_item.MenuItem(
                int(row[0]), (row[1]), str(row[2]), colors=self._app._settings['ui']['color'])
            menu_item.setClickedHandle(self.menuItemClicked)

            items[cat[row[3]]].append(menu_item)
            # items.append(ui.menu_item.MenuItem(
            # int(row[0]), (row[1]), str(row[2])))

        # print(items)

        return items

    def addOrderItem(self, item, order_item):
        """Add product to current order

        Args:
            item (int): product ID
            order_item (OrderItem): order item object
        """
        # Create new order if there is no current order
        if self.currentOrderId is None:
            # print(self._app._user._id)
            self.currentOrderId = self._app._user._db.insert(
                'insert into zamowienia_dzisiaj (idpracownika) values (%s)', [(str(self._app._user._id),)])
            print(f'[D] Created order no. {self.currentOrderId}.')

        item_id = str(item)
        if item_id in self._ui._orderItems.keys():
            self.orderIncr(item_id)
        else:
            val = [(str(self.currentOrderId), str(item), str(1))]
            self._app._user._db.insert(
                'insert into zawartosczamowienia values (%s, %s, %s)', val)
            self._ui._orderItems[item_id] = order_item
            self._ui._orderItems[item_id].buttons['incr'].clicked.connect(
                partial(self.orderIncr, item))
            self._ui._orderItems[item_id].buttons['decr'].clicked.connect(
                partial(self.orderDecr, item))
            self._ui._orderItems[item_id].buttons['remove'].clicked.connect(
                partial(self.orderRemove, item))
            self._ui.order.widget().layout().addWidget(order_item)
            self.updateOrderSum()

        print(
            f"[D] Added '{order_item._label}' to order no. {self.currentOrderId}")

    def orderIncr(self, item_id):
        self._ui._orderItems[str(item_id)].increment()
        self._app._user._db.update("update zawartosczamowienia set liczba={} where (idzamowienia={} and idproduktu={})".format(
            self._ui._orderItems[str(item_id)]._amount, self.currentOrderId, item_id))
        self.updateOrderSum()

    def orderDecr(self, item_id):
        self._ui._orderItems[str(item_id)].decrement()
        if self._ui._orderItems[str(item_id)]._amount > 0:
            self._app._user._db.update("update zawartosczamowienia set liczba={} where (idzamowienia={} and idproduktu={})".format(
                self._ui._orderItems[str(item_id)]._amount, self.currentOrderId, item_id))
            self.updateOrderSum()

        else:
            self.orderRemove(item_id)

    def orderRemove(self, item_id):
        # Remove widget from layout by setting its parent to None
        print(self._ui._orderItems[str(item_id)]._label)
        print(
            f"[D] Removed '{self._ui._orderItems[str(item_id)]._label}' from order no. {self.currentOrderId}.")
        self._ui._orderItems[str(item_id)].setParent(None)
        # Delete from dict
        del self._ui._orderItems[str(item_id)]
        # Delete from database
        self._app._user._db.delete("delete from zawartosczamowienia where (idzamowienia={} and idproduktu={})".format(
            self.currentOrderId, item_id))
        self.updateOrderSum()

    def updateOrderSum(self):
        q = self._app._user._db.query(
            f'select wartoscrachunku from zamowienia where idzamowienia={self.currentOrderId}')
        print(q)
        print(self.currentOrderId)
        self._ui.sum.setText(str(q[0][0]))

    def openPaymentWidget(self):
        # Check if order is open
        if self.currentOrderId is None:
            print("[W] There is no order to close/finish.")
            return

        # Open widget with payment options
        self.payment = self.createPaymentWidget()
        widget_width = self._app._settings['window']['width']//3
        self.payment.setMinimumWidth(widget_width)
        self.payment.show()

    def openOrderManagementWidget(self):
        print('[D] Open orders widget')
        self.orderManagement = self.createOrderManagementWidget()
        widget_width = self._app._settings['window']['width']//2
        widget_height = self._app._settings['window']['height']//2
        self.orderManagement.setMinimumWidth(widget_width)
        self.orderManagement.setMinimumHeight(widget_height)
        self.orderManagement.show()
        pass

    def finishOrder(self):

        # Call remote proc with args
        args = [str(self.currentOrderId), str(self.payment_type.checkedId())]
        self._app._user._db.callProcedure('zrealizuj_zamowienie', args)
        # Hide finished order
        for key, val in self._ui._orderItems.items():
            val.setParent(None)
        # remove items from dict
        self._ui._orderItems.clear()

        print(
            f"[D] Order no. {self.currentOrderId} is closed, payment type id:{self.payment_type.checkedId()}")
        # There is no current open order now
        self.currentOrderId = None
        # Clear sum label text
        self._ui.sum.setText('')
        self.payment.hide()

    def createPaymentWidget(self):
        widget = qt.QDialog(self._ui)
        widget.setWindowTitle('Zamówienie')
        layout = qt.QGridLayout()
        widget.setLayout(layout)

        layout.addWidget(qt.QLabel('Wybierz typ płatności:'))

        payment_button = {}
        self.payment_type = qt.QButtonGroup(widget)

        q = self._app._user._db.query('select * from typplatnosci;')
        for item in q:
            payment_button[item[1]] = qt.QRadioButton(item[1])
            self.payment_type.addButton(payment_button[item[1]], int(item[0]))
            layout.addWidget(payment_button[item[1]])

        payment_button['gotowka'].setChecked(True)

        finish_button = qt.QPushButton('zrealizuj')
        finish_button.clicked.connect(self.finishOrder)
        layout.addWidget(finish_button)

        return widget

    def createOrderManagementWidget(self):
        widget = qt.QDialog(self._ui)
        widget.setWindowTitle('Otwarte rachunki')
        layout = qt.QVBoxLayout()
        widget.setLayout(layout)

        layout.addWidget(
            qt.QLabel(f"{'nr':^3}{'data otwarcia':^25}{'wartość':^10}"))

        list_widget = qt.QListWidget()
        q = self._app._user._db.query(
            'select * from zamowienia_dzisiaj where czasplatnosci IS NULL;')
        for item in q:
            list_item_text = f"{item[0]:>3}{str(item[1]):>25}{str(item[4]):>10}"
            list_item = qt.QListWidgetItem(list_item_text)
            list_widget.addItem(list_item)
            if item[0] == self.currentOrderId:
                list_widget.setCurrentItem(list_item)

        list_widget.itemClicked.connect(self.changeCurrentOrder)
        layout.addWidget(list_widget)

        return widget

    def changeCurrentOrder(self, item):
        if self.currentOrderId == int(item.text().split()[0]):
            self.orderManagement.hide()
            return

        if self.currentOrderId != None:
            q = self._app._user._db.query(
                f'select wartoscrachunku from zamowienia where idzamowienia={self.currentOrderId}')
            if int(q[0][0]) == 0:
                self.removeOrder(self.currentOrderId)
            else:
                # Update order area
                # Clear order area
                for key, val in self._ui._orderItems.items():
                    val.setParent(None)
                self._ui._orderItems.clear()

        # Change current order id
        self.currentOrderId = int(item.text().split()[0])
        print(f'[D] changed order to {self.currentOrderId}')
        self.updateOrderSum()

        # Add items in current order
        q = self._app._user._db.query(
            f"select produkty.idproduktu, nazwa, cena, liczba from zawartosczamowienia, produkty where idzamowienia={self.currentOrderId} and zawartosczamowienia.idproduktu=produkty.idproduktu;")

        for item in q:
            # print(item)
            order_item = ui.OrderItem(
                item[0], item[1], str(item[2]), int(item[3]), colors=self._app._settings['ui']['color'])

            item_id = str(item[0])
            # print(item_id)
            self._ui._orderItems[item_id] = order_item
            self._ui._orderItems[item_id].buttons['incr'].clicked.connect(
                partial(self.orderIncr, item[0]))
            self._ui._orderItems[item_id].buttons['decr'].clicked.connect(
                partial(self.orderDecr, item[0]))
            self._ui._orderItems[item_id].buttons['remove'].clicked.connect(
                partial(self.orderRemove, item[0]))
            self._ui.order.widget().layout().addWidget(order_item)

        # print(self._ui._orderItems)
        self.orderManagement.hide()

    def go_backHandle(self):
        self._app.setUI('main_menu')
        # remove order if opened
        self.cleanup()

    def removeOrder(self, order_id):
        # Remove order from database
        self._app._user._db.delete(
            f"delete from zamowienia_dzisiaj where idzamowienia={order_id}")
        print(f"[D] Removed order {order_id} from database.")
        # remove items from dict
        for key, val in self._ui._orderItems.items():
            val.setParent(None)
        # remove items from dict
        self._ui._orderItems.clear()

    def editMenuHandle(self):
        print('[D] clicked "edit" button')

        # Toggle edit mode
        if self.edit_mode == False:
            # Toggle 'new item' button
            self.edit_mode = True
            self._ui.buttons['new'].show()
            self._ui.buttons['edit'].setStyleSheet(styles.style['edit-menu-on'])
            # Toggle menu items edit mode
            self.toggleMenuItemsMode(True)
        else:
            # Toggle 'new item' button
            self.edit_mode = False
            self._ui.buttons['new'].hide()
            self._ui.buttons['edit'].setStyleSheet(styles.style['edit-menu-off'])
            # Toggle menu items edit mode
            self.toggleMenuItemsMode(False)

    def toggleMenuItemsMode(self, enable):
        for tab in self._ui._menu_items:
            for item in self._ui._menu_items[tab]:
                item.enableEditMode(enable)


    def newMenuItemHandle(self):
        print('[D] clicked "new" button')
        self._ui.buttons['new'].show()
        # Open new menu item dialog
        self.dialog = self.createAddMenuItemWidget()
        widget_width = self._app._settings['window']['width']//3
        widget_height = self._app._settings['window']['height']//4
        self.dialog.setMinimumWidth(widget_width)
        self.dialog.setMinimumHeight(widget_height)
        self.dialog.show()

    def createAddMenuItemWidget(self):
        widget = qt.QDialog(self._ui)
        widget.setWindowTitle('Dodaj nowy produkt')
        layout = qt.QGridLayout()

        nazwa = qt.QLineEdit()
        nazwa.setPlaceholderText('Nazwa')
        nazwa.setValidator(qtgui.QRegExpValidator(qtcore.QRegExp('[^;\-\'"]*')))

        cena = qt.QLineEdit()
        cena.setPlaceholderText('Cena')
        cena.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 2, widget))

        typ = qt.QComboBox()
        q = self._app._user._db.query('select * from typproduktu;')
        for item in q:
            typ.addItem(item[1])

        dodaj = qt.QPushButton('dodaj')
        dodaj.clicked.connect(self.addMenuItem)

        layout.addWidget(nazwa, 0, 0)
        layout.addWidget(cena, 1, 0)
        layout.addWidget(typ, 2, 0)
        layout.addWidget(dodaj, 3, 0)

        widget.setLayout(layout)
        return widget

    def editMenuItemHandle(self, id):
        print(f'[D] edit menu item {id}')
        # Open edit menu item dialog
        self.dialog = self.createEditMenuItemWidget(id)
        widget_width = self._app._settings['window']['width']//3
        widget_height = self._app._settings['window']['height']//4
        self.dialog.setMinimumWidth(widget_width)
        self.dialog.setMinimumHeight(widget_height)
        self.dialog.show()
        # self.reloadMenuArea()

    def createEditMenuItemWidget(self, id):
        widget = qt.QDialog(self._ui)
        widget.setWindowTitle(f'Modyfikuj produkt (id {id})')
        layout = qt.QGridLayout()

        prod_q = self._app._user._db.query(f'select * from produkty where idproduktu={id};')
        # print(prod_q)

        nazwa = qt.QLineEdit()
        nazwa.setText(prod_q[0][1])
        nazwa.setValidator(qtgui.QRegExpValidator(qtcore.QRegExp('[^;\-\'"]*')))

        cena = qt.QLineEdit()
        cena.setText(str(prod_q[0][2]).replace('.', ','))
        cena.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 2, widget))

        typ = qt.QComboBox()
        q = self._app._user._db.query('select * from typproduktu;')
        for item in q:
            typ.addItem(item[1])
        typ.setCurrentIndex(int(prod_q[0][3])-1)

        modyfikuj = qt.QPushButton('Zmodyfikuj')
        modyfikuj.clicked.connect(partial(self.modifyMenuItem, id))
        skladniki = qt.QPushButton('Składniki')
        skladniki.clicked.connect(partial(self.openProductIngredients, id))


        layout.addWidget(nazwa, 0, 0)
        layout.addWidget(cena, 1, 0)
        layout.addWidget(typ, 2, 0)
        layout.addWidget(skladniki, 3, 0)
        layout.addWidget(modyfikuj, 4, 0)

        widget.setLayout(layout)
        return widget

    def deleteMenuItemHandle(self, id):
        print(f'[D] delete menu item {id}')
        # Delete menu item
        self._app._user._db.delete(f'delete from produkty where idproduktu={id}')
        self.reloadMenuArea()

    def connectEditModeButton(self, edit_mode=False):
        for tab in self._ui._menu_items:
            for item in self._ui._menu_items[tab]:
                item.button['edit'].clicked.connect(partial(self.editMenuItemHandle, item._id))
                item.button['delete'].clicked.connect(partial(self.deleteMenuItemHandle, item._id))
        self._ui.buttons['edit'].clicked.connect(self.editMenuHandle)
        self._ui.buttons['new'].clicked.connect(self.newMenuItemHandle)
        if edit_mode == True:
            self.edit_mode = False
            self.editMenuHandle()

    def reloadMenuArea(self):
        current_tab = self._ui.menu.currentIndex()
        # Clear menu area
        self._ui.menu.setParent(None)
        # Reload menu area
        self._ui.menu = self._ui._createMenuArea(self.getMenuItems())
        # self._ui.layout.removeItem(self._ui.layout.itemAtPosition(1, 2))
        self._ui.layout.addWidget(self._ui.menu, 1, 2, 11, 3)
        self._ui.menu.setCurrentIndex(current_tab)
        self._ui.menu.setStyleSheet(styles.style['menu-area'])
        self.connectEditModeButton(True)

    def addMenuItem(self):
        name = self.dialog.layout().itemAtPosition(0,0).widget().text()
        print(name)
        price = self.dialog.layout().itemAtPosition(1,0).widget().text().replace(',', '.')
        print(price)
        cat_id = self.dialog.layout().itemAtPosition(2,0).widget().currentIndex()+1
        print(cat_id)
        # , nazwa.text(), cena.text(), typ.currentIndex()
        self._app._user._db.insert("insert into produkty (nazwa, cena, idtypu) values (%s, %s, %s);", [(name, price, cat_id,)])
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None
        self.reloadMenuArea()
    
    def modifyMenuItem(self, id):
        name = self.dialog.layout().itemAtPosition(0,0).widget().text()
        print(name)
        price = self.dialog.layout().itemAtPosition(1,0).widget().text().replace(',', '.')
        print(price)
        cat_id = self.dialog.layout().itemAtPosition(2,0).widget().currentIndex()+1
        print(cat_id)
        # , nazwa.text(), cena.text(), typ.currentIndex()
        self._app._user._db.update(f"update produkty set nazwa='{name}', cena={price}, idtypu={cat_id} where idproduktu={id};")
        self.dialog.hide()
        self.dialog.setParent(None)
        self.dialog = None
        self.reloadMenuArea()

    def openProductIngredients(self, id):
        self.ingredients = self.createIngredientsDialog(id)
        widget_width = self._app._settings['window']['width']//2
        widget_height = self._app._settings['window']['height']//2
        self.ingredients.setMinimumWidth(widget_width)
        self.ingredients.setMinimumHeight(widget_height)
        self.ingredients.show()

    def createIngredientsDialog(self, id):
        widget = qt.QDialog(self.dialog)
        widget.setWindowTitle(f'Modyfikuj produkt (id {id})')
        layout = qt.QGridLayout()

        list_widget = qt.QListWidget()
        monospace_font = qtgui.QFontDatabase.systemFont(qtgui.QFontDatabase.FixedFont)
        list_widget.setFont(monospace_font)
        q = self._app._user._db.query(f'select skladniki.idskladnika, nazwa, liczba, jednostka from skladproduktu, skladniki where skladproduktu.idskladnika=skladniki.idskladnika and idproduktu={id};')

        for item in q:
            list_item_text = f"id {item[0]:>3} {item[1]:<25}{str(item[2]):>6} {str(item[3]):<4}"
            list_item = qt.QListWidgetItem(list_item_text)
            list_widget.addItem(list_item)

        create_button = qt.QPushButton('Nowy')
        modify_button = qt.QPushButton('Zmień')
        delete_button = qt.QPushButton('Usuń')

        # list_widget.itemDoubleClicked.connect()
        create_button.clicked.connect(partial(self.addIngredientProduct, id))
        modify_button.clicked.connect(partial(self.modifyIngredientProduct, id))
        delete_button.clicked.connect(partial(self.deleteIngredientProduct, id))
        list_widget.itemDoubleClicked.connect(partial(self.modifyIngredientProduct, id))

        layout.addWidget(list_widget, 0, 0, 5, 3)
        layout.addWidget(create_button, 5, 0, 1, 1)
        layout.addWidget(modify_button, 5, 1, 1, 1)
        layout.addWidget(delete_button, 5, 2, 1, 1)

        widget.setLayout(layout)
        return widget

    def addIngredientProduct(self, prod_id):
        print(prod_id)
        # ingredient_id = self.ingredients.layout().itemAtPosition(0, 0).widget().currentItem().text().split()[1]
        # print(ingredient_id)
        self.ingredient_dialog = self.createAddIngredientsDialog(prod_id)
        widget_width = self._app._settings['window']['width']//2
        widget_height = self._app._settings['window']['height']//2
        self.ingredient_dialog.setMinimumWidth(widget_width)
        self.ingredient_dialog.setMinimumHeight(widget_height)
        self.ingredient_dialog.show()

    def modifyIngredientProduct(self, prod_id):
        ingredient_id = self.ingredients.layout().itemAtPosition(0, 0).widget().currentItem().text().split()[1]
        self.ingredient_dialog = self.createModifyIngredientsDialog(prod_id, ingredient_id)
        widget_width = self._app._settings['window']['width']//2
        widget_height = self._app._settings['window']['height']//2
        self.ingredient_dialog.setMinimumWidth(widget_width)
        self.ingredient_dialog.setMinimumHeight(widget_height)
        self.ingredient_dialog.show()

    def deleteIngredientProduct(self, prod_id):
        # print(prod_id)
        ingredient_id = self.ingredients.layout().itemAtPosition(0, 0).widget().currentItem().text().split()[1]
        self._app._user._db.delete(f"delete from skladproduktu where (idproduktu={prod_id} and idskladnika={ingredient_id});")
        self.updateProductIngredientsWidget(prod_id)

    def createModifyIngredientsDialog(self, prod_id, ingr_id):
        widget = qt.QDialog(self.ingredients)
        widget.setWindowTitle(f'Skladnik (id {ingr_id}) w produkcie {prod_id}')
        layout = qt.QGridLayout()

        print(ingr_id)
        ingr_q = self._app._user._db.query(f'select * from skladniki where idskladnika={ingr_id};')
        ingr_prod_q = self._app._user._db.query(f'select * from skladproduktu where idskladnika={ingr_id} and idproduktu={prod_id};')

        nazwa = qt.QLabel(ingr_q[0][1])
        liczba = qt.QLineEdit()
        liczba.setText(str(ingr_prod_q[0][2]).replace('.', ','))
        jednostka = qt.QLabel(f"{ingr_q[0][3]}")
        liczba.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 4, widget))

        modyfikuj = qt.QPushButton('Zmodyfikuj')
        modyfikuj.clicked.connect(partial(self.modifyProductIngredient, prod_id, ingr_id))

        layout.addWidget(nazwa, 0, 0, 1, 2)
        layout.addWidget(liczba, 1, 0, 1, 1)
        layout.addWidget(jednostka, 1, 1, 1, 1)
        layout.addWidget(modyfikuj, 3, 0, 1, 2)

        widget.setLayout(layout)
        return widget

    def createAddIngredientsDialog(self, prod_id):
        widget = qt.QDialog(self.ingredients)
        widget.setWindowTitle(f'Nowy skladnik w produkcie {prod_id}')
        layout = qt.QGridLayout()

        ingr_q = self._app._user._db.query(f'select idskladnika, nazwa, jednostka from skladniki where idskladnika not in (select idskladnika from skladproduktu where idproduktu={prod_id});')

        skladniki = qt.QComboBox(widget)
        # skladniki.
        for item in ingr_q:
            skladniki.addItem(item[1])
        liczba = qt.QLineEdit()
        jednostka = qt.QLabel(f"{ingr_q[0][2]}")
        liczba.setValidator(qtgui.QDoubleValidator(0.0, 100000.0, 4, widget))

        dodaj = qt.QPushButton('Dodaj')
        dodaj.clicked.connect(partial(self.addProductIngredient, prod_id))

        layout.addWidget(skladniki, 0, 0, 1, 2)
        layout.addWidget(liczba, 1, 0, 1, 1)
        layout.addWidget(jednostka, 1, 1, 1, 1)
        layout.addWidget(dodaj, 2, 0, 1, 2)

        widget.setLayout(layout)
        return widget

    def modifyProductIngredient(self, prod_id, ingr_id):
        val = self.ingredient_dialog.layout().itemAtPosition(1, 0).widget().text().replace(',', '.')
        self._app._user._db.update(f"update skladproduktu set liczba={val} where idproduktu={prod_id} and idskladnika={ingr_id};")
        self.updateProductIngredientsWidget(prod_id)
        self.ingredient_dialog.hide()
        self.ingredient_dialog.setParent(None)
        self.ingredient_dialog = None

    def addProductIngredient(self, prod_id):
        ingr = self.ingredient_dialog.layout().itemAtPosition(0, 0).widget().currentText()
        print(ingr)
        ingr_id = self._app._user._db.query(f'select idskladnika from skladniki where nazwa="{ingr}"')[0][0]
        val = self.ingredient_dialog.layout().itemAtPosition(1, 0).widget().text().replace(',', '.')
        print(val)
        self._app._user._db.insert("insert into skladproduktu values (%s, %s, %s);", [(prod_id, ingr_id, val,)])
        self.updateProductIngredientsWidget(prod_id)
        self.ingredient_dialog.hide()
        self.ingredient_dialog.setParent(None)
        self.ingredient_dialog = None

    def updateProductIngredientsWidget(self, id):
        list_widget = qt.QListWidget()
        monospace_font = qtgui.QFontDatabase.systemFont(qtgui.QFontDatabase.FixedFont)
        list_widget.setFont(monospace_font)
        q = self._app._user._db.query(f'select skladniki.idskladnika, nazwa, liczba, jednostka from skladproduktu, skladniki where skladproduktu.idskladnika=skladniki.idskladnika and idproduktu={id};')

        for item in q:
            list_item_text = f"id {item[0]:>3} {item[1]:<25}{str(item[2]):>6} {str(item[3]):<4}"
            list_item = qt.QListWidgetItem(list_item_text)
            list_widget.addItem(list_item)

        self.ingredients.layout().itemAtPosition(0, 0).widget().setParent(None)
        self.ingredients.layout().addWidget(list_widget, 0, 0, 5, 3)

    def createNewOrder(self):
        self.currentOrderId = self._app._user._db.insert(
            'insert into zamowienia_dzisiaj (idpracownika) values (%s)', [(str(self._app._user._id),)])
        print(f'[D] Created order no. {self.currentOrderId}.')

        for key, val in self._ui._orderItems.items():
            val.setParent(None)
        # remove items from dict
        self._ui._orderItems.clear()

        # Clear sum label text
        self._ui.sum.setText('')