#!/usr/bin/python3

import sys
import core
import json

def load_json(path):
    with open(path, "r") as f:
        return json.load(f)

def main():
    settings = load_json("app.conf")
    # Create app
    app = core.App(settings)
    # Set initial view
    app.setUI('login')
    # Show window
    app.show()
    # Run app
    sys.exit(app.run())


if __name__ == "__main__":
    main()
