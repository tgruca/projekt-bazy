
import mysql.connector as sql


class Database:
    """Database connector.
    """

    def __init__(self, settings):
        self._settings = settings

    def connect(self):
        """Connect to the database

        Args:
            user (string): user name
            passwd (string): user password
        """
        try:
            self._db_con = sql.connect(
                host=self._settings['host'],
                user=self._settings['user'],
                password=self._settings['password'],
                database=self._settings['db-name'])
            return True
        except sql.Error as e:
            print(e)
            return False

    def query(self, query):
        """Execute sql query and return rows.

        Args:
            query (string): sql query

        Returns:
            list of tuples: fields in row
        """
        with self._db_con.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            return result

    def insert(self, query, values):
        """Insert values into database 

        Args:
            query (string): sql query
            values (list of tuples): values to insert matching used sql query
        """
        # self._db_con.cursor().res
        with self._db_con.cursor() as cursor:

            try:
                cursor.executemany(query, values)
                self._db_con.commit()
                # print("[D] {} records inserted.".format(cursor.rowcount))

                return int(cursor.lastrowid)
            except sql.Error as e:
                print(e)

    def update(self, query):
        """Update values in database 

        Args:
            query (string): sql query
        """
        with self._db_con.cursor() as cursor:

            try:
                cursor.execute(query)
                self._db_con.commit()
                # print("[D] {} records updated.".format(cursor.rowcount))

                return int(cursor.lastrowid)
            except sql.Error as e:
                print(e)

    def delete(self, query):
        """delete row in database 

        Args:
            query (string): sql query
        """
        with self._db_con.cursor() as cursor:

            try:
                cursor.execute(query)
                self._db_con.commit()
                # print("[D] {} records deleted.".format(cursor.rowcount))

                return int(cursor.lastrowid)
            except sql.Error as e:
                print(e)

    def callProcedure(self, proc_name, args):
        """Call stored procedure.

        Args:
            proc_name (string): procedure name to call
        """
        with self._db_con.cursor() as cursor:

            try:
                print(f'[D] calling {proc_name} stored procedure')
                cursor.callproc(proc_name, args)
                self._db_con.commit()

            except sql.Error as e:
                print(e)
