@echo off

:: Set variables
set rootdir=%cd%
set srcdir=%cd%\db

:: Switch dir
cd %srcdir%
:: echo %srcdir%

:: Run sql scripts
mysql --user="root" --password="root" -e "source pizzeria.sql"

:: Return back to root dir
cd %rootdir%
set "rootdir="
set "srcdir="