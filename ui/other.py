from functools import partial
import PyQt5.QtWidgets as qt
import database
import PyQt5.QtCore as qtcore
# import PyQt5.QtGui as gui


class Produkt(qt.QWidget):
    def __init__(self, label):
        super().__init__()
        self._label = label
        lab = qt.QLabel(label)
        lab.setAlignment(qtcore.Qt.AlignCenter)
        l = qt.QGridLayout()
        l.addWidget(lab, 0, 0)
        self.setLayout(l)

        self.setMinimumHeight(100)

        self.setStyleSheet(
            'background-color: red; border-width: 2px; \
            border-color: black; border-style: solid;')

    def mousePressEvent(self, event):
        super().mousePressEvent(event)

        print('clicked {}'.format(self._label))


class Login(qt.QWidget):
    """
    Login interface for application
    """

    def __init__(self):
        """Create Login UI object

        Args:
            app (App): Handle to Qt application object
        """
        super().__init__()

        self.setLayout(self.createLayout())

    def createLayout(self):
        self.buttons = {'login': qt.QPushButton('login')}

        layout = qt.QGridLayout()
        layout.addWidget(self.buttons['login'], 0, 0)
        layout.addWidget(qt.QLabel('login screen'), 1, 0)

        return layout


class Main(qt.QWidget):
    """
    Main application interface. 
    Showing database data and allow to update it.
    """

    def __init__(self):
        """Create Main UI.
        """
        super().__init__()

        # self.setLayout(self.createLayout())

    def createLayout(self, db):
        """Create UI layout
        """
        # Fetch product names
        q = db.query('select nazwa from produkty;')

        # Create Produkt objects
        items = []
        for row in q:
            items.append(Produkt(row[0]))

        # Create widget with products
        produkty = qt.QWidget()
        # produkty.setStyleSheet("background-color: coral")

        prod_layout = qt.QGridLayout()
        produkty.setLayout(prod_layout)
        

        # Create layout
        prod_layout.setSpacing(5)
        # Add items into layout
        for i in range(len(items)-1):
            prod_layout.addWidget(items[i], i//3, i % 3)

        

        # Put widget in scroll area
        scrollarea = qt.QScrollArea()
        scrollarea.setWidgetResizable(True)
        scrollarea.setWidget(produkty)

        

        # Create final layout
        layout = qt.QGridLayout()
        layout.addWidget(qt.QLabel('main screen'), 0, 0)
        layout.addWidget(scrollarea, 1, 0)

        self.setLayout(layout)
        return layout


# app = qtwidgets.QApplication(sys.argv)
# # app.setStyle('Fusion')
# window = qtwidgets.QWidget()


# def layout1():
#     st_layout.setCurrentIndex(0)

# def layout2():
#     st_layout.setCurrentIndex(1)
#     # window.setLayout(la2)

# la1 = qtwidgets.QGridLayout()
# btn = qtwidgets.QPushButton('button')
# btn.clicked.connect(layout2)
# la1.addWidget(btn, 0, 0)
# # la1.addWidget(qtwidgets.QPushButton('button'), 0, 0)
# la1.addWidget(qtwidgets.QLabel('<h1>Layout 1</h1>'), 0, 1)

# la2 = qtwidgets.QGridLayout()
# btn = qtwidgets.QPushButton('button')
# btn.clicked.connect(layout1)
# la2.addWidget(btn, 0, 0)
# la2.addWidget(qtwidgets.QLabel('<h1>Layout 2</h1>'), 0, 1)

# st_layout = qtwidgets.QStackedLayout()
# page1 = qtwidgets.QWidget()
# page1.setLayout(la1)
# page2 = qtwidgets.QWidget()
# page2.setLayout(la2)

# st_layout.addWidget(page1)
# st_layout.addWidget(page2)
