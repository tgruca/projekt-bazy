import PyQt5.QtWidgets as qt
from PyQt5.QtGui import *
import PyQt5.QtCore as qtcore
import decimal

import styles


class MenuItem(qt.QWidget):
    """Item that is placed in menu area.
    Holds information about product and allow to add product to current order.
    """

    def __init__(self, id,  label, price, colors={}):
        """Create menu item

        Args:
            id (int): product id
            label (string): product's name
            price (string): product's price
        """
        super().__init__()

        self._id = id
        self._label = label
        self._price = price
        self.color = colors

        self.button = {}

        # Put content widget to item in layout
        content_layout = qt.QBoxLayout(qt.QBoxLayout.BottomToTop)
        content_layout.addWidget(self._createItemContent(label, price))
        # margin of single item
        content_layout.setContentsMargins(qtcore.QMargins(0,0,0,0))
        content_layout.setSpacing(5)
        # self.setContentsMargins(qtcore.QMargins(0,0,0,0))

        self.setLayout(content_layout)

        # Minimal item height
        # self.setMinimumHeight(80)
        # self.setMaximumHeight(120)
        self.setFixedHeight(80)
        # self.setMinimumWidth(200)
        # self.setMaximumWidth(200)

    def _createItemContent(self, label, price):
        """Create widget with item content

        Args:
            label (string): product name
            price (string): product price

        Returns:
            QWidget: content widget
        """
        content = qt.QWidget(self)
        content.setStyleSheet(styles.style['menu-item'])
        
        # Product name
        self.lab = qt.QLabel(label)
        self.lab.setAlignment(qtcore.Qt.AlignCenter)
        self.lab.setWordWrap(True)
        # Product price
        self.pr = qt.QLabel(price)
        self.pr.setAlignment(qtcore.Qt.AlignRight | qtcore.Qt.AlignVCenter)
        self.pr.setMargin(3)
        self.pr.setStyleSheet(
            f" background-color: {self.color['secondaryDark']};")

        # Create edit mode buttons
        self.button['edit'] = qt.QPushButton('E')
        self.button['delete'] = qt.QPushButton('X')
        self.button['edit'].hide()
        self.button['delete'].hide()
        
        # Create content layout and put labels on it
        l = qt.QGridLayout()
        l.addWidget(self.lab, 0, 0, 2, 5)
        l.addWidget(self.button['edit'], 2, 1, 1, 1)
        l.addWidget(self.button['delete'], 2, 2, 1, 1)
        l.addWidget(self.pr, 2, 3, 1, 2)
        l.setSpacing(5)
        # Set layout to content widget
        content.setLayout(l)

        return content

    def setClickedHandle(self, handle_func):
        self._clickedHandle = handle_func

    def mousePressEvent(self, event):
        """Function called when MenuItem is pressed

        Args:
            event (QEvent (?)): Qt event object
        """
        super().mousePressEvent(event)
        # print('clicked {}'.format(self._label))
        # call clicked item handle function
        self._clickedHandle(self._id)

    def enterEvent(self, event):
        super().enterEvent(event)
        # print(event)
        self.lab.setStyleSheet(f"QWidget > QLabel {{ background-color: {self.color['secondaryLight']};}}")
        self.pr.setStyleSheet(f"QWidget > QLabel {{ background-color: {self.color['secondary']};}}")
        # self.lab.setProperty('hover', 'true')
    
    def leaveEvent(self, event):
        # print(event)
        super().leaveEvent(event)
        self.lab.setStyleSheet(f"QWidget > QLabel {{ background-color: {self.color['secondary']};}}")
        self.pr.setStyleSheet(f"QWidget > QLabel {{ background-color: {self.color['secondaryDark']};}}")
        # self.lab.setProperty('hover', '')
        
    def enableEditMode(self, enable):
        if enable:
            # Show edit controls
            self.button['edit'].show()
            self.button['delete'].show()
        else:
            # Hide edit controls
            self.button['edit'].hide()
            self.button['delete'].hide()