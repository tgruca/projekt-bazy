from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class OknoMainMenu(QWidget):
    def __init__(self):
        super().__init__()

        # self.resize(1010,768)
        # self.setWindowTitle("Menu główne")

        # Create buttons
        self.buttons = {}
        self.buttons['menu'] = QPushButton('Menu',self)
        self.buttons['produkty'] = QPushButton("Magazyn\nskładników",self)
        self.buttons['zamowienia'] = QPushButton('Historia \n zamówień',self)
        self.buttons['wyloguj'] = QPushButton('Wyloguj',self)

        # Set buttons size and position
        self.buttons['menu'].move(50,100)
        self.buttons['produkty'].move(370,100)
        self.buttons['zamowienia'].move(690,100)
        self.buttons['wyloguj'].move(330,570)

        self.buttons['menu'].resize(270,400)
        self.buttons['produkty'].resize(270,400)
        self.buttons['zamowienia'].resize(270,400)
        self.buttons['wyloguj'].resize(350, 120)

        # Set font for button labels
        self.buttons['menu'].setFont(QFont('Arial',25))
        self.buttons['produkty'].setFont(QFont('Arial', 25))
        self.buttons['zamowienia'].setFont(QFont('Arial', 25))
        self.buttons['wyloguj'].setFont(QFont('Arial', 18))