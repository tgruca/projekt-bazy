import PyQt5.QtWidgets as qt
import PyQt5.QtCore as qtcore


class ZamowieniaItem(qt.QWidget):

    def __init__(self, idzamowienia, czaszlozenia, czasplatnosci, idTypu, wartoscrachunku, idpracownika):

        super().__init__()

        self._idzamowienia = idzamowienia
        self._czaszlozenia = czaszlozenia
        self._czasplatnosci = czasplatnosci
        self._idTypu = idTypu
        self._wartoscrachunku = wartoscrachunku
        self._idpracownika = idpracownika

        content_layout = qt.QBoxLayout(qt.QBoxLayout.BottomToTop)
        content_layout.addWidget(self._createItemContent(czaszlozenia, czasplatnosci, idTypu, wartoscrachunku, idpracownika))

        self.setLayout(content_layout)


    def _createItemContent(self, czaszlozenia, czasplatnosci, idTypu, wartoscrachunku, idpracownika):

        content = qt.QWidget(self)


        #l_idzamowienia = qt.QLabel(idzamowienia)
        #l_idzamowienia.setAlignment(qtcore.Qt.AlignCenter)

        l_czaszlozenia = qt.QLabel('<b>Czas złożenia:</b> '+czaszlozenia)
        l_czaszlozenia.setAlignment(qtcore.Qt.AlignLeft)

        l_czasplatnosci = qt.QLabel('<b>Czas płatności:</b> '+czasplatnosci)
        l_czasplatnosci.setAlignment(qtcore.Qt.AlignLeft)

        l_idTypu = qt.QLabel('<b>ID typu płatności:</b> '+idTypu)
        l_idTypu.setAlignment(qtcore.Qt.AlignLeft)

        l_wartoscrachunku = qt.QLabel('<b>Wartość rachunku:</b> '+wartoscrachunku)
        l_wartoscrachunku.setAlignment(qtcore.Qt.AlignLeft)

        l_idpracownika = qt.QLabel('<b>ID pracownika:</b> '+idpracownika)
        l_idpracownika.setAlignment(qtcore.Qt.AlignLeft)

        l = qt.QGridLayout()
        l.addWidget(l_czaszlozenia)
        l.addWidget(l_czasplatnosci)
        l.addWidget(l_idTypu)
        l.addWidget(l_wartoscrachunku)
        l.addWidget(l_idpracownika)


        content.setLayout(l)
        content.setStyleSheet(
             "background-color: #ff99ce;")

        return content

    #def setClickedHandle(self, handle_func):
        #self._clickedHandle = handle_func

    #def mousePressEvent(self, event):
        """Function called when ZamowieniaItem is pressed

        Args:
            event (QEvent (?)): Qt event object
        """
      #  super().mousePressEvent(event)

        # print('clicked {}'.format(self._label))
        # call clicked item handle function
      #  self._clickedHandle(self._id)