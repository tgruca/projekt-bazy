import PyQt5.QtWidgets as qt
import PyQt5.QtCore as qtcore
import decimal

import styles
import ui


class OknoMenu(qt.QWidget):
    """Menu UI class.
    Let user choose products from menu and add them to order.
    User can modify or finish order.
    """

    def __init__(self, settings):
        """Create menu UI.
        """
        super().__init__()

        self._settings = settings
        self.color = settings['color']

        self._orderItems = {}
        self.buttons = {}

    def createLayout(self, db):
        """Create menu UI layout

        Args:
            db (datbase.Database): connection to database

        Returns:
            QGridLayout: interface layout
        """
        self.layout = qt.QGridLayout()
        self.setLayout(self.layout)
        self.layout.setContentsMargins(qtcore.QMargins(0, 0, 0, 0))
        self.layout.setSpacing(0)

        # Create interface areas
        # Order area
        self.order = self._createOrderList()
        self.order_area = self._createOrderArea(self.order)
        self.order_area.setStyleSheet(styles.style['order-area'])
        
        self.order.setStyleSheet(styles.style['order'])
        # Menu area
        self.menu = self._createMenuArea(db)
        
        self.menu.setStyleSheet(styles.style['menu-area'])

        
        # self.menu.setStyleSheet("background-color: coral;")
        # Button bar area
        self.button_bar = self._createButtonBar()
        self.button_bar.setStyleSheet(f"background-color: {self.color['primaryDark']}; color: {self.color['primaryText']};")

        # Set created areas in layout
        self.layout.addWidget(self.order_area, 1, 0, 11, 2)
        self.layout.addWidget(self.menu, 1, 2, 11, 3)
        self.layout.addWidget(self.button_bar, 0, 0, 1, 5)

        return self.layout

    def _createMenuArea(self, menu_items):
        """Create UI layout

        Args:
            menu_items (list of MenuItem): Menu items

        Returns:
            QWidget: Menu area widget
        """
        self._menu_items = menu_items
        menuarea = qt.QTabWidget(self)
        menuarea.setStyleSheet(styles.style['menu-area'])

        for key, val in menu_items.items():
            menuarea.addTab(self._createMenuTab(val), key)

        # Add edit button for admin
        self.buttons['edit'] = qt.QPushButton('edit', menuarea)
        self.buttons['new'] = qt.QPushButton('new', menuarea)
        self.buttons['edit'].setStyleSheet(styles.style['edit-menu-off'])
        self.buttons['edit'].raise_()
        self.buttons['edit'].move(560, 650)
        self.buttons['edit'].show()
        self.buttons['new'].setStyleSheet(styles.style['edit-menu-off'])
        self.buttons['new'].raise_()
        self.buttons['new'].move(510, 650)
        self.buttons['new'].hide()


        return menuarea

    def _createMenuTab(self, menu_items):
        # Create widget with products
        produkty = qt.QWidget()
        prod_layout = qt.QGridLayout()
        blank = qt.QWidget()
        blank.setFixedHeight(1)
        prod_layout.addWidget(blank, 0, 0, 1, 3)
        prod_layout.setAlignment(qtcore.Qt.AlignTop)
        produkty.setLayout(prod_layout)

        # Create layout
        # Add items into layout
        for i in range(len(menu_items)):
            prod_layout.addWidget(menu_items[i], (i//3)+1, i % 3)

        # margin of whole area
        prod_layout.setContentsMargins(qtcore.QMargins(10,10,10,10))
        prod_layout.setSpacing(10)
        
        # Put widget in scroll area
        scrollarea = qt.QScrollArea()
        scrollarea.setWidgetResizable(True)
        scrollarea.setWidget(produkty)

        return scrollarea

    def _createOrderList(self):
        """[summary]

        Returns:
            QWidget: Order area widget
        """
        zamowienie = qt.QWidget()
        layout = qt.QVBoxLayout()
        layout.setAlignment(qtcore.Qt.AlignTop)
        zamowienie.setContentsMargins(qtcore.QMargins(0, 0, 0, 0))
        layout.setContentsMargins(qtcore.QMargins(0, 0, 0, 0))
        layout.setSpacing(0)
        zamowienie.setLayout(layout)

        # margin of whole area
        layout.setContentsMargins(qtcore.QMargins(5, 5, 5, 5))
        layout.setSpacing(0)

        # Put widget in scroll area
        scrollarea = qt.QScrollArea()
        scrollarea.setContentsMargins(qtcore.QMargins(0, 0, 0, 0))
        scrollarea.setWidgetResizable(True)
        scrollarea.setWidget(zamowienie)

        return scrollarea

    def _createOrderArea(self, order):

        podsumowanie = qt.QWidget()
        pods_layout = qt.QHBoxLayout()
        self.buttons['zrealizuj'] = qt.QPushButton('zrealizuj')
        self.sum = qt.QLabel('')
        pods_layout.addWidget(self.buttons['zrealizuj'])
        pods_layout.addWidget(self.sum)
        podsumowanie.setLayout(pods_layout)

        orderarea = qt.QWidget()
        area_layout = qt.QGridLayout()
        area_layout.addWidget(order, 0, 0, 15, 1)
        area_layout.addWidget(podsumowanie, 16, 0, 1, 1)
        orderarea.setLayout(area_layout)

        return orderarea

    def _createButtonBar(self):
        """[summary]

        Returns:
            QWidget: Order area widget
        """
        # print('creating buttonbar')
        buttonbar = qt.QWidget()
        layout = qt.QGridLayout()
        buttonbar.setLayout(layout)
        layout.setContentsMargins(qtcore.QMargins(2, 2, 2, 2))
        

        # Add buttons
        self.buttons['go_back'] = qt.QPushButton('wróć')
        self.buttons['go_back'].setStyleSheet(styles.style['menu-button'])
        self.buttons['go_back'].setMaximumWidth(100)
        self.buttons['go_back'].setMinimumHeight(40)

        self.buttons['orders'] = qt.QPushButton('rachunki')
        self.buttons['orders'].setStyleSheet(styles.style['menu-button'])
        self.buttons['orders'].setMaximumWidth(100)
        self.buttons['orders'].setMinimumHeight(40)

        self.buttons['new-order'] = qt.QPushButton('Nowy rachunek')
        self.buttons['new-order'].setStyleSheet(styles.style['menu-button'])
        self.buttons['new-order'].setMaximumWidth(150)
        self.buttons['new-order'].setMinimumHeight(40)

        layout.addWidget(self.buttons['go_back'], 0, 6, 1, 1)
        layout.addWidget(self.buttons['orders'], 0, 2, 1, 1)
        layout.addWidget(self.buttons['new-order'], 0, 1, 1, 1)
        layout.addWidget(qt.QWidget(), 0, 3, 1, 3)
        layout.addWidget(qt.QWidget(), 0, 0, 1, 1)
        layout.addWidget(qt.QWidget(), 0, 7, 1, 1)



        return buttonbar