############################
# UPRAWNIENIA UŻYTKOWNIKOW
############################

# dostep aplikacji do bazy uzytkownikow
CREATE USER IF NOT EXISTS 'bazy.app'@'localhost' IDENTIFIED BY 'app';
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'bazy.app'@'localhost';
GRANT SELECT ON pizzeria.pracownicy TO 'bazy.app'@'localhost';
-- FLUSH PRIVILEGES;

# pracownicy
CREATE USER IF NOT EXISTS 'bazy.pracownik1'@'localhost' IDENTIFIED BY 'pracownik1';
CREATE USER IF NOT EXISTS 'bazy.pracownik2'@'localhost' IDENTIFIED BY 'pracownik2';
CREATE USER IF NOT EXISTS 'bazy.pracownik3'@'localhost' IDENTIFIED BY 'pracownik3';
CREATE USER IF NOT EXISTS 'bazy.pracownik4'@'localhost' IDENTIFIED BY 'pracownik4';

REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON pizzeria.zamowienia_dzisiaj TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON pizzeria.zawartosczamowienia TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT ON pizzeria.zamowienia TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT ON pizzeria.produkty TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT ON pizzeria.typproduktu TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT ON pizzeria.skladniki TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT ON pizzeria.typplatnosci TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT SELECT ON pizzeria.pracownicy TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
GRANT EXECUTE ON PROCEDURE pizzeria.zrealizuj_zamowienie TO 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';
-- REVOKE SELECT ON pizzeria.pracownicy FROM 'bazy.pracownik1'@'localhost', 'bazy.pracownik2'@'localhost', 'bazy.pracownik3'@'localhost', 'bazy.pracownik4'@'localhost';

# kierownicy/administratorzy
CREATE USER IF NOT EXISTS 'bazy.admin'@'localhost' IDENTIFIED BY 'admin';
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'bazy.admin'@'localhost';
GRANT ALL PRIVILEGES ON pizzeria.* TO 'bazy.admin'@'localhost';
FLUSH PRIVILEGES;