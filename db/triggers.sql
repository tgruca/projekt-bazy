############################
# TRIGGERY
############################


# wstaw czas zrealizowania zamowienia
DROP TRIGGER IF EXISTS wstaw_czas_zlozenia;
CREATE TRIGGER wstaw_czas_zlozenia
	BEFORE INSERT ON zamowienia 
	FOR EACH ROW
	SET NEW.czaszlozenia = CURRENT_TIMESTAMP();


# Aktualizuj wartość zamówienia
DROP TRIGGER IF EXISTS dodaj_wartosc_i;
DELIMITER $$
CREATE TRIGGER dodaj_wartosc_i
AFTER INSERT
ON zawartosczamowienia FOR EACH ROW
BEGIN
	DECLARE val DECIMAL(6,2);

	IF czy_zrealizowane(NEW.idzamowienia) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Zamowienie zostalo zreaizowane';
	END IF;	
	SELECT cena INTO val FROM produkty WHERE produkty.idproduktu=NEW.idproduktu;
	UPDATE zamowienia SET wartoscrachunku=wartoscrachunku+val*NEW.liczba WHERE zamowienia.idzamowienia = NEW.idzamowienia;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS dodaj_wartosc_u;
DELIMITER $$
CREATE TRIGGER dodaj_wartosc_u
AFTER UPDATE
ON zawartosczamowienia FOR EACH ROW
BEGIN
	DECLARE val DECIMAL(6,2);
	DECLARE dif INT;

	IF czy_zrealizowane(NEW.idzamowienia) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Zamowienie zostalo zreaizowane';
	END IF;
	SELECT NEW.liczba-OLD.liczba INTO dif;
	SELECT cena INTO val FROM produkty WHERE produkty.idproduktu=NEW.idproduktu;	
	UPDATE zamowienia SET wartoscrachunku=wartoscrachunku+val*dif WHERE zamowienia.idzamowienia = NEW.idzamowienia;
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS dodaj_wartosc_d;
DELIMITER $$
CREATE TRIGGER dodaj_wartosc_d
AFTER DELETE
ON zawartosczamowienia FOR EACH ROW
BEGIN
	DECLARE val DECIMAL(6,2);
	
	IF czy_zrealizowane(OLD.idzamowienia) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Zamowienie zostalo zreaizowane';
	END IF;
	SELECT cena INTO val FROM produkty WHERE produkty.idproduktu=OLD.idproduktu;
	UPDATE zamowienia SET wartoscrachunku=wartoscrachunku-val*OLD.liczba WHERE zamowienia.idzamowienia = OLD.idzamowienia;
END$$
DELIMITER ;
