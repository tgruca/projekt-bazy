DROP DATABASE IF EXISTS pizzeria;
CREATE DATABASE pizzeria;
USE pizzeria;

source tables.sql
source data.sql
source triggers.sql
source procedures.sql
source perm.sql